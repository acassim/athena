/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "../ITkPixelRawDataProviderTool.h"
#include "../ITkPixelRodDecoder.h"


DECLARE_COMPONENT( ITkPixelRawDataProviderTool )  
DECLARE_COMPONENT( ITkPixelRodDecoder )
